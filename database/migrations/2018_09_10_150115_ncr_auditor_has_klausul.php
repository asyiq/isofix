<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NcrAuditorHasKlausul extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('ncr_has_klausul_iso', function (Blueprint $table){
        $table->integer('id_klausul_iso');
        $table->integer('id_ncr_auditor');

        $table->foreign('id_klausul_iso')->references('id_klausul_iso')->on('klausul_iso');
        $table->foreign('id_ncr_auditor')->references('id_ncr_auditor')->on('ncr_auditor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
