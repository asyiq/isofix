<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObservationHasKlausul extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('obserservation_has_klausul', function (Blueprint $table) {
            // $table->increments('id_ohs');
            $table->integer('observation_id');
            $table->integer('klausul_iso_id');

            $table->foreign('observation_id')->references('id_observation')->on('observation');
            $table->foreign('klausul_iso_id')->references('id_klausul_iso')->on('klausul_iso');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
